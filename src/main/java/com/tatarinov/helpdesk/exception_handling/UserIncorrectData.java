package com.tatarinov.helpdesk.exception_handling;

import lombok.Data;

@Data
public class UserIncorrectData{
    private String info;
}