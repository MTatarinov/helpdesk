package com.tatarinov.helpdesk.service;

import com.tatarinov.helpdesk.model.entity.Ticket;

import javax.mail.MessagingException;

public interface EmailService {
    void sendMail(Ticket ticket, String fileName) throws MessagingException;
}
