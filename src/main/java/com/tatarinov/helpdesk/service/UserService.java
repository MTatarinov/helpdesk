package com.tatarinov.helpdesk.service;

import com.tatarinov.helpdesk.dto.UserDto;
import com.tatarinov.helpdesk.model.entity.User;

import java.util.List;

public interface UserService {
    List<UserDto> getAllUsers();
    UserDto getUser(Long id);
    String[] getEmailForAllManagers();
    String[] getEmailForAllEngineersAndOwner(User userOwner);
}