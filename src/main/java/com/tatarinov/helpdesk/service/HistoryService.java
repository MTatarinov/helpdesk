package com.tatarinov.helpdesk.service;

import com.tatarinov.helpdesk.dto.HistoryDto;
import com.tatarinov.helpdesk.model.entity.Ticket;
import com.tatarinov.helpdesk.model.entity.User;

import java.util.List;

public interface HistoryService {
    List<HistoryDto> getAllHistoriesForTicket(Long ticketId);
    HistoryDto getHistory(Long id);
    void saveHistory(HistoryDto historyDto, Long ticketId);
    void addHistory(Ticket ticket, User user, String status);
    void addHistory(Ticket ticket, User user, String status, String fileName);
    void addHistory(Ticket ticket, User user,
                           String oldState, String newState,
                           String status);
}