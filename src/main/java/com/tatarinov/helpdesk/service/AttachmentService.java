package com.tatarinov.helpdesk.service;

import com.tatarinov.helpdesk.dto.AttachmentDto;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface AttachmentService {
    AttachmentDto getAttachment(Long id);
    AttachmentDto getAttachmentForTicket(Long ticketId);
    void saveAttachment(MultipartFile file, Long ticketId) throws IOException;
    void deleteAttachment(Long id);
}
