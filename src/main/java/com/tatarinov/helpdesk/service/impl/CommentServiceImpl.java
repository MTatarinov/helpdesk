package com.tatarinov.helpdesk.service.impl;

import com.tatarinov.helpdesk.dao.CommentDao;
import com.tatarinov.helpdesk.dao.TicketDao;
import com.tatarinov.helpdesk.dto.CommentDto;
import com.tatarinov.helpdesk.exception_handling.NoSuchUserException;
import com.tatarinov.helpdesk.model.entity.Comment;
import com.tatarinov.helpdesk.model.entity.Ticket;
import com.tatarinov.helpdesk.service.CommentService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@Service
public class CommentServiceImpl implements CommentService {

    private final ModelMapper modelMapper;
    private final CommentDao commentDao;
    private final TicketDao ticketDao;

    @Override
    @Transactional
    public List<CommentDto> getAllCommentsForTicket(Long ticketId) {
        Ticket ticket = ticketDao.getEntity(ticketId)
                .orElseThrow();
        List<CommentDto> allCommentsDto = new ArrayList<>();
        List<Comment> allCommentsForTicket = commentDao.getAllCommentsForTicket(ticket);
        for (Comment comment : allCommentsForTicket) {
            allCommentsDto.add(modelMapper.map(comment, CommentDto.class));
        }
        return allCommentsDto;
    }

    @Override
    @Transactional
    public CommentDto getComment(Long id) {
        String message = "There is not comment with ID = " + id + " in database.";
        Comment comment = commentDao.getEntity(id)
                .orElseThrow(() -> new NoSuchUserException(message));
        return modelMapper.map(comment, CommentDto.class);
    }

    @Override
    @Transactional
    public void saveComment(CommentDto commentDto, Long ticketId) {
        Comment comment = modelMapper.map(commentDto, Comment.class);
        Ticket ticket = ticketDao.getEntity(ticketId)
                .orElseThrow();

        comment.setTicket(ticket);
        comment.setUser(ticket.getUserOwner());

        commentDao.saveEntity(comment);
    }

    @Override
    @Transactional
    public void updateComment(CommentDto commentDto, Long commentId) {
        Comment comment = commentDao.getEntity(commentId)
                .orElseThrow();

        comment.setText(commentDto.getText());

        commentDao.saveEntity(comment);
    }
}