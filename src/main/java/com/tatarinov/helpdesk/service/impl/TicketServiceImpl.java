package com.tatarinov.helpdesk.service.impl;

import com.tatarinov.helpdesk.dao.TicketDao;
import com.tatarinov.helpdesk.dao.UserDao;
import com.tatarinov.helpdesk.dto.TicketDto;
import com.tatarinov.helpdesk.exception_handling.NoSuchUserException;
import com.tatarinov.helpdesk.model.entity.Ticket;
import com.tatarinov.helpdesk.model.entity.User;
import com.tatarinov.helpdesk.model.enums.Category;
import com.tatarinov.helpdesk.model.enums.Role;
import com.tatarinov.helpdesk.model.enums.State;
import com.tatarinov.helpdesk.model.enums.Urgency;
import com.tatarinov.helpdesk.service.EmailService;
import com.tatarinov.helpdesk.service.HistoryService;
import com.tatarinov.helpdesk.service.TicketService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.MessagingException;
import java.util.*;

@AllArgsConstructor
@Service
public class TicketServiceImpl implements TicketService {

    private final ModelMapper modelMapper;
    private final TicketDao ticketDao;
    private final UserDao userDao;
    private final EmailService emailService;
    private final HistoryService historyService;

    private static final String TICKET_CREATED = "Ticket is created";
    private static final String TICKET_EDITED = "Ticket is edited";
    private static final String TICKET_CHANGED = "Ticket Status is changed";

    private final Map<Key, String> messages = new HashMap<>();

    {
        messages.put(new Key(State.DRAFT.name(), State.NEW.name()), "draft_or_decline_to_new");
        messages.put(new Key(State.DECLINED.name(), State.NEW.name()), "draft_or_decline_to_new");
        messages.put(new Key(State.NEW.name(), State.APPROVED.name()), "new_to_approved");
        messages.put(new Key(State.NEW.name(), State.DECLINED.name()), "new_to_declined");
        messages.put(new Key(State.NEW.name(), State.CANCELLED.name()), "new_to_cancelled");
        messages.put(new Key(State.APPROVED.name(), State.CANCELLED.name()), "approved_to_cancelled");
        messages.put(new Key(State.IN_PROGRESS.name(), State.DONE.name()), "approved_to_cancelled");
    }

    @Override
    @Transactional
    public List<TicketDto> getAllTicketsForUser(Long userId) {
        User user = userDao.getEntity(userId).orElseThrow();
        Role role = user.getRole();
        List<Ticket> allTickets = new ArrayList<>();
        List<TicketDto> allTicketsDto = new ArrayList<>();

        if (role.equals(Role.EMPLOYEE)) allTickets = ticketDao.getAllTicketsForEmployee(user);
        if (role.equals(Role.ENGINEER)) allTickets = ticketDao.getAllTicketsForEngineer(user);
        if (role.equals(Role.MANAGER)) allTickets = ticketDao.getAllTicketsForManager(user);

        for (Ticket ticket : allTickets) {
            allTicketsDto.add(modelMapper.map(ticket, TicketDto.class));
        }

        return allTicketsDto;
    }

    @Override
    @Transactional
    public TicketDto getTicket(long id) {
        String message = "There is not ticket with ID = " + id + " in database.";
        Ticket ticket = ticketDao.getEntity(id)
                .orElseThrow(() -> new NoSuchUserException(message));

        return modelMapper.map(ticket, TicketDto.class);
    }

    @Override
    @Transactional
    public void saveTicket(TicketDto ticketDto, Long userId) {
        Ticket ticket = modelMapper.map(ticketDto, Ticket.class);
        User user = userDao.getEntity(userId).orElseThrow();

        ticket.setUserOwner(user);

        ticketDao.saveEntity(ticket);
        historyService.addHistory(ticket, user, TICKET_CREATED);
    }

    @Override
    @Transactional
    public void updateTicket(TicketDto ticketDto, Long ticketId) {
        Ticket ticket = ticketDao.getEntity(ticketId)
                .orElseThrow();
        User user = ticket.getUserOwner();

        ticket.setName(ticketDto.getName());
        ticket.setDescription(ticketDto.getDescription());
        ticket.setCategory(Category.valueOf(ticketDto.getCategory()));
        ticket.setUrgency(Urgency.valueOf(ticketDto.getUrgency()));

        ticketDao.saveEntity(ticket);
        historyService.addHistory(ticket, user, TICKET_EDITED);
    }

    @Override
    @Transactional
    public void updateTicketState(TicketDto ticketDto, Long ticketId, Long userId) {
        Ticket ticket = ticketDao.getEntity(ticketId)
                .orElseThrow();
        User user = userDao.getEntity(userId).orElseThrow();
        String oldState = ticket.getState().name();
        String newState = ticketDto.getState();

        if (oldState.equals(State.NEW.name()) && newState.equals(State.APPROVED.name())) {
            ticket.setUserApprover(user);
        }
        if (oldState.equals(State.APPROVED.name()) && newState.equals(State.IN_PROGRESS.name())) {
            ticket.setUserAssignee(user);
        }

        ticket.setState(State.valueOf(ticketDto.getState()));

        ticketDao.saveEntity(ticket);
        historyService.addHistory(ticket, user, oldState, newState, TICKET_CHANGED);

        String message = messages.get(new Key(oldState, newState));

        if (message != null) {
            try {
                emailService.sendMail(ticket, message);
            } catch (MessagingException e) {
                e.printStackTrace();
            }
        }
    }

    class Key {
        private final String oldState;
        private final String newState;

        public Key(String oldState, String newState) {
            this.oldState = oldState;
            this.newState = newState;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Key key = (Key) o;
            return Objects.equals(oldState, key.oldState) && Objects.equals(newState, key.newState);
        }

        @Override
        public int hashCode() {
            return Objects.hash(oldState, newState);
        }
    }
}