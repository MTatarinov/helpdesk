package com.tatarinov.helpdesk.service.impl;

import com.tatarinov.helpdesk.dao.AttachmentDao;
import com.tatarinov.helpdesk.dao.TicketDao;
import com.tatarinov.helpdesk.dto.AttachmentDto;
import com.tatarinov.helpdesk.exception_handling.NoSuchUserException;
import com.tatarinov.helpdesk.model.entity.Attachment;
import com.tatarinov.helpdesk.model.entity.Ticket;
import com.tatarinov.helpdesk.model.entity.User;
import com.tatarinov.helpdesk.service.AttachmentService;
import com.tatarinov.helpdesk.service.HistoryService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@AllArgsConstructor
@Service
public class AttachmentServiceImpl implements AttachmentService {

    private final ModelMapper modelMapper;
    private final AttachmentDao attachmentDao;
    private final TicketDao ticketDao;
    private final HistoryService historyService;

    private static final String FILE_ATTACHED = "File is attached";
    private static final String FILE_REMOVED = "File is removed";

    @Override
    @Transactional
    public AttachmentDto getAttachment(Long id) {
        String message = "There is not attachment with ID = " + id + " in database.";
        Attachment attachment = attachmentDao.getEntity(id)
                .orElseThrow(() -> new NoSuchUserException(message));

        return modelMapper.map(attachment, AttachmentDto.class);
    }

    @Override
    @Transactional
    public AttachmentDto getAttachmentForTicket(Long ticketId) {
        Ticket ticket = ticketDao.getEntity(ticketId)
                .orElseThrow();
        Attachment attachment = attachmentDao.getAttachmentForTicket(ticket)
                .orElseThrow();

        return modelMapper.map(attachment, AttachmentDto.class);
    }

    @Override
    @Transactional
    public void saveAttachment(MultipartFile file, Long ticketId) throws IOException {
        Ticket ticket = ticketDao.getEntity(ticketId)
                .orElseThrow();
        User user = ticket.getUserOwner();

        String fileName = StringUtils.cleanPath(file.getOriginalFilename());

        Attachment attachment = new Attachment();
        attachment.setName(fileName);
        attachment.setBlob(file.getBytes());
        attachment.setContentType(file.getContentType());
        attachment.setTicket(ticket);

        attachmentDao.saveEntity(attachment);
        historyService.addHistory(ticket, user, FILE_ATTACHED, fileName);
    }

    @Override
    @Transactional
    public void deleteAttachment(Long id) {
        Attachment attachment = attachmentDao.getEntity(id)
                .orElseThrow();
        Ticket ticket = attachment.getTicket();
        User user = ticket.getUserOwner();

        attachmentDao.deleteAttachment(id);
        historyService.addHistory(ticket, user, FILE_REMOVED, attachment.getName());
    }
}
