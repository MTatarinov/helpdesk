package com.tatarinov.helpdesk.service.impl;

import com.tatarinov.helpdesk.model.entity.Ticket;
import com.tatarinov.helpdesk.model.entity.User;
import com.tatarinov.helpdesk.service.EmailService;
import com.tatarinov.helpdesk.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.*;

@AllArgsConstructor
@Service
public class EmailServiceImpl implements EmailService {

    private final JavaMailSender mailSender;
    private final TemplateEngine templateEngine;
    private final UserService userService;

    @Async
    @Override
    public void sendMail(Ticket ticket, String fileName) throws MessagingException {
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true, "UTF-8");

        Map<String, Object> variables = new HashMap<>();
        variables.put("id", ticket.getId());

        createMessage(helper, variables, ticket, fileName);

        String text = templateEngine.process(fileName, new Context(Locale.getDefault(), variables));
        helper.setText(text, true);

        mailSender.send(message);
    }

    private void createMessage(MimeMessageHelper helper, Map<String, Object> variables,
                               Ticket ticket, String fileName) throws MessagingException {
        User userOwner = ticket.getUserOwner();
        User userApprover = ticket.getUserApprover();
        User userAssignee = ticket.getUserAssignee();

        switch (fileName) {
            case "draft_or_decline_to_new":
                helper.setSubject("New ticket for approval");
                helper.setTo(userService.getEmailForAllManagers());
                break;
            case "new_to_approved":
                helper.setSubject("Ticket was approved");
                helper.setTo(userService.getEmailForAllEngineersAndOwner(userOwner));
                break;
            case "new_to_declined":
                variables.put("firstName", userOwner.getFirstName());
                variables.put("lastName", userOwner.getLastName());
                helper.setSubject("Ticket was declined");
                helper.setTo(userOwner.getEmail());
                break;
            case "new_to_cancelled":
                variables.put("firstName", userOwner.getFirstName());
                variables.put("lastName", userOwner.getLastName());
                helper.setSubject("Ticket was cancelled");
                helper.setTo(userOwner.getEmail());
                break;
            case "approved_to_cancelled":
                helper.setSubject("Ticket was cancelled");
                helper.setTo(new String[]{userOwner.getEmail(), userApprover.getEmail()});
                break;
            case "in_progress_to_done":
                variables.put("firstName", userOwner.getFirstName());
                variables.put("lastName", userOwner.getLastName());
                helper.setSubject("Ticket was done");
                helper.setTo(userOwner.getEmail());
                break;
            case "feedback":
                variables.put("firstName", userAssignee.getFirstName());
                variables.put("lastName", userAssignee.getLastName());
                helper.setSubject("Feedback was provided");
                helper.setTo(userAssignee.getEmail());
                break;
        }
    }
}