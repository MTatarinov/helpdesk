package com.tatarinov.helpdesk.service.impl;

import com.tatarinov.helpdesk.dao.HistoryDao;
import com.tatarinov.helpdesk.dao.TicketDao;
import com.tatarinov.helpdesk.dto.HistoryDto;
import com.tatarinov.helpdesk.exception_handling.NoSuchUserException;
import com.tatarinov.helpdesk.model.entity.History;
import com.tatarinov.helpdesk.model.entity.Ticket;
import com.tatarinov.helpdesk.model.entity.User;
import com.tatarinov.helpdesk.service.HistoryService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

@AllArgsConstructor
@Service
public class HistoryServiceImpl implements HistoryService {

    private final ModelMapper modelMapper;
    private final HistoryDao historyDao;
    private final TicketDao ticketDao;

    @Override
    @Transactional
    public List<HistoryDto> getAllHistoriesForTicket(Long ticketId) {
        Ticket ticket = ticketDao.getEntity(ticketId)
                .orElseThrow();
        List<HistoryDto> allHistoriesDto = new ArrayList<>();
        List<History> allHistoriesForTicket = historyDao.getAllHistoriesForTicket(ticket);
        for (History history : allHistoriesForTicket) {
            allHistoriesDto.add(modelMapper.map(history, HistoryDto.class));
        }
        return allHistoriesDto;
    }

    @Override
    @Transactional
    public HistoryDto getHistory(Long id) {
        String message = "There is not history with ID = " + id + " in database.";
        History history = historyDao.getEntity(id)
                .orElseThrow(() -> new NoSuchUserException(message));

        return modelMapper.map(history, HistoryDto.class);
    }

    @Override
    @Transactional
    public void saveHistory(HistoryDto historyDto, Long ticketId) {
        History history = modelMapper.map(historyDto, History.class);
        Ticket ticket = ticketDao.getEntity(ticketId)
                .orElseThrow();

        history.setTicket(ticket);
        history.setUser(ticket.getUserOwner());

        historyDao.saveEntity(history);
    }

    @Override
    @Transactional
    public void addHistory(Ticket ticket, User user, String status) {
        History history = new History();
        StringBuilder description = getDescription(user, status);

        history.setTicket(ticket);
        history.setUser(user);
        history.setAction(status);
        history.setDescription(description.toString());

        historyDao.saveEntity(history);
    }

    @Override
    @Transactional
    public void addHistory(Ticket ticket, User user, String status, String fileName) {
        History history = new History();
        StringBuilder description = getDescription(user, status)
                .append(": ")
                .append(fileName);

        history.setTicket(ticket);
        history.setUser(user);
        history.setAction(status);
        history.setDescription(description.toString());

        historyDao.saveEntity(history);
    }

    @Override
    @Transactional
    public void addHistory(Ticket ticket, User user,
                           String oldState, String newState,
                           String status) {
        History history = new History();
        StringBuilder description = getDescription(user, status)
                .append(" from ")
                .append(oldState)
                .append(" to ")
                .append(newState);

        history.setTicket(ticket);
        history.setUser(user);
        history.setAction(status);
        history.setDescription(description.toString());

        historyDao.saveEntity(history);
    }

    private StringBuilder getDescription(User user, String status) {
        String date = new SimpleDateFormat("MMM dd, yyyy HH:mm:ss", Locale.ENGLISH)
                .format(new Date());
        StringBuilder description = new StringBuilder();

        description
                .append(date)
                .append(" - ")
                .append(user.getFirstName())
                .append(" ")
                .append(user.getLastName())
                .append(" - ")
                .append(status)
                .append(" - ")
                .append(status);

        return description;
    }
}