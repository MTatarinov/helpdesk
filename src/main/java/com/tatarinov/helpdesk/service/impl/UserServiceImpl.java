package com.tatarinov.helpdesk.service.impl;

import com.tatarinov.helpdesk.dao.UserDao;
import com.tatarinov.helpdesk.dto.UserDto;
import com.tatarinov.helpdesk.exception_handling.NoSuchUserException;
import com.tatarinov.helpdesk.model.entity.User;
import com.tatarinov.helpdesk.service.UserService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@Service
public class UserServiceImpl implements UserService {

    private final ModelMapper modelMapper;
    private final UserDao userDao;

    @Override
    @Transactional
    public List<UserDto> getAllUsers() {
        List<UserDto> allUsersDto = new ArrayList<>();
        List<User> allUsers = userDao.getAllUsers();
        for (User user : allUsers) {
            allUsersDto.add(modelMapper.map(user, UserDto.class));
        }
        return allUsersDto;
    }

    @Override
    @Transactional
    public UserDto getUser(Long id) {
        String message = "There is not user with ID = " + id + " in database.";
        User user = userDao.getEntity(id)
                .orElseThrow(() -> new NoSuchUserException(message));

        return modelMapper.map(user, UserDto.class);
    }

    @Override
    @Transactional
    public String[] getEmailForAllManagers() {
        List<User> users = userDao.getAllManagers();

        return users
                .stream()
                .map(User::getEmail)
                .toArray(String[]::new);
    }

    @Override
    @Transactional
    public String[] getEmailForAllEngineersAndOwner(User userOwner) {
        List<User> users = userDao.getAllEngineers();
        users.add(userOwner);

        return users
                .stream()
                .map(User::getEmail)
                .toArray(String[]::new);
    }
}