package com.tatarinov.helpdesk.service.impl;

import com.tatarinov.helpdesk.dao.FeedbackDao;
import com.tatarinov.helpdesk.dao.TicketDao;
import com.tatarinov.helpdesk.dto.FeedbackDto;
import com.tatarinov.helpdesk.exception_handling.NoSuchUserException;
import com.tatarinov.helpdesk.model.entity.Feedback;
import com.tatarinov.helpdesk.model.entity.Ticket;
import com.tatarinov.helpdesk.service.EmailService;
import com.tatarinov.helpdesk.service.FeedbackService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.MessagingException;

@AllArgsConstructor
@Service
public class FeedbackServiceImpl implements FeedbackService {

    private final ModelMapper modelMapper;
    private final FeedbackDao feedbackDao;
    private final TicketDao ticketDao;
    private final EmailService emailService;

    @Override
    @Transactional
    public FeedbackDto getFeedbackForTicket(Long ticketId) {
        Ticket ticket = ticketDao.getEntity(ticketId)
                .orElseThrow();
        Feedback feedback = feedbackDao.getFeedbackForTicket(ticket)
                .orElseThrow();
        return modelMapper.map(feedback, FeedbackDto.class);
    }

    @Override
    @Transactional
    public FeedbackDto getFeedback(Long id) {
        String message = "There is not feedback with ID = " + id + " in database.";
        Feedback feedback = feedbackDao.getEntity(id)
                .orElseThrow(() -> new NoSuchUserException(message));
        return modelMapper.map(feedback, FeedbackDto.class);
    }

    @Override
    @Transactional
    public void saveFeedback(FeedbackDto feedbackDto, Long ticketId) {
        Feedback feedback = modelMapper.map(feedbackDto, Feedback.class);
        Ticket ticket = ticketDao.getEntity(ticketId)
                .orElseThrow();

        feedback.setTicket(ticket);
        feedback.setUser(ticket.getUserOwner());

        feedbackDao.saveEntity(feedback);
        try {
            emailService.sendMail(ticket, "feedback");
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    @Override
    @Transactional
    public void updateFeedback(FeedbackDto feedbackDto, Long feedbackId) {
        Feedback feedback = feedbackDao.getEntity(feedbackId)
                .orElseThrow();

        feedback.setRate(feedbackDto.getRate());
        feedback.setText(feedbackDto.getText());

        feedbackDao.saveEntity(feedback);
    }
}