package com.tatarinov.helpdesk.service;

import com.tatarinov.helpdesk.dto.CommentDto;

import java.util.List;

public interface CommentService {
    List<CommentDto> getAllCommentsForTicket(Long ticketId);
    CommentDto getComment(Long id);
    void saveComment(CommentDto commentDto, Long ticketId);
    void updateComment(CommentDto commentDto, Long commentId);
}
