package com.tatarinov.helpdesk.service;

import com.tatarinov.helpdesk.dto.TicketDto;

import java.util.List;

public interface TicketService {
    List<TicketDto> getAllTicketsForUser(Long userId);
    void saveTicket(TicketDto ticketDto, Long userId);
    void updateTicket(TicketDto ticketDto, Long ticketId);
    void updateTicketState(TicketDto ticketDto, Long ticketId, Long userId);
    TicketDto getTicket(long id);
}
