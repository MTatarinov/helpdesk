package com.tatarinov.helpdesk.service;

import com.tatarinov.helpdesk.dto.FeedbackDto;

public interface FeedbackService {
    FeedbackDto getFeedbackForTicket(Long ticketId);
    FeedbackDto getFeedback(Long id);
    void saveFeedback(FeedbackDto feedbackDto, Long ticketId);
    void updateFeedback(FeedbackDto feedbackDto, Long feedbackId);
}