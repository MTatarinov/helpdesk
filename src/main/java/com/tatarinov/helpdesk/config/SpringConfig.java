package com.tatarinov.helpdesk.config;

import com.tatarinov.helpdesk.dto.TicketDto;
import com.tatarinov.helpdesk.model.entity.Ticket;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.thymeleaf.templateresolver.ITemplateResolver;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

@Configuration
@EnableWebMvc
@ComponentScan("com.tatarinov.helpdesk")
public class SpringConfig {

    @Bean
    public CommonsMultipartResolver multipartResolver(){
        CommonsMultipartResolver resolver = new CommonsMultipartResolver();
        resolver.setMaxUploadSize(5242880);
        return resolver;
    }

    @Bean
    public ModelMapper modelMapper() {
        ModelMapper modelMapper = new ModelMapper();

        Converter<Date, String> formatDateString = ctx -> ctx.getSource() != null
                ? new SimpleDateFormat("dd/MM/yyyy").format(ctx.getSource())
                : "";
        Converter<String, Date> formatStringDate = ctx -> {
            try {
                return ctx.getSource() != null
                        ? new SimpleDateFormat("dd/MM/yyyy").parse(ctx.getSource())
                        : new Date();
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return new Date();
        };

        modelMapper.typeMap(Ticket.class, TicketDto.class)
                        .addMappings(mapper -> mapper.using(formatDateString)
                        .map(Ticket::getDesiredResolutionDate, TicketDto::setDesiredResolutionDate));
        modelMapper.typeMap(TicketDto.class, Ticket.class)
                        .addMappings(mapper -> mapper.using(formatStringDate)
                        .map(TicketDto::getDesiredResolutionDate, Ticket::setDesiredResolutionDate));
        return modelMapper;
    }

    @Bean
    public JavaMailSender getJavaMailSender()
    {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost("smtp.gmail.com");
        mailSender.setPort(587);

        mailSender.setUsername("helpdesk201190@gmail.com");
        mailSender.setPassword("12admin34");

        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.debug", "true");

        return mailSender;
    }

    @Bean
    public ITemplateResolver templateResolver()
    {
        ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
        templateResolver.setPrefix("message/");
        templateResolver.setSuffix(".html");
        templateResolver.setTemplateMode(TemplateMode.HTML);

        return templateResolver;
    }

    @Bean
    public TemplateEngine templateEngine()
    {
        TemplateEngine templateEngine = new TemplateEngine();
        templateEngine.setTemplateResolver(this.templateResolver());

        return templateEngine;
    }

}