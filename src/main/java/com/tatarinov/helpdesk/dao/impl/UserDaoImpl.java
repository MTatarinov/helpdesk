package com.tatarinov.helpdesk.dao.impl;

import com.tatarinov.helpdesk.dao.AbstractRepository;
import com.tatarinov.helpdesk.dao.UserDao;
import com.tatarinov.helpdesk.model.entity.User;
import com.tatarinov.helpdesk.model.enums.Role;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class UserDaoImpl extends AbstractRepository<User> implements UserDao {

    protected UserDaoImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public List<User> getAllUsers() {

        return sessionFactory
                .getCurrentSession()
                .createQuery("from User", User.class)
                .getResultList();
    }

    @Override
    public Optional<User> getUserByEmail(String email) {

        return Optional.ofNullable((User) sessionFactory
                .getCurrentSession()
                .createQuery("from User u where u.email = :email")
                .setParameter("email", email)
                .getSingleResult());
    }

    @Override
    public List<User> getAllManagers() {

        return sessionFactory
                .getCurrentSession()
                .createQuery("from User u where u.role = :role")
                .setParameter("role", Role.MANAGER)
                .getResultList();
    }

    @Override
    public List<User> getAllEngineers() {

        return sessionFactory
                .getCurrentSession()
                .createQuery("from User u where u.role = :role")
                .setParameter("role", Role.ENGINEER)
                .getResultList();
    }
}