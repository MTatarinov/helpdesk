package com.tatarinov.helpdesk.dao.impl;

import com.tatarinov.helpdesk.dao.AbstractRepository;
import com.tatarinov.helpdesk.dao.FeedbackDao;
import com.tatarinov.helpdesk.model.entity.Feedback;
import com.tatarinov.helpdesk.model.entity.Ticket;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class FeedbackDaoImpl extends AbstractRepository<Feedback> implements FeedbackDao {

    protected FeedbackDaoImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public Optional<Feedback> getFeedbackForTicket(Ticket ticket) {

        return Optional.ofNullable((Feedback) sessionFactory
                .getCurrentSession()
                .createQuery("from Feedback f where f.ticket = :ticket")
                .setParameter("ticket", ticket)
                .getSingleResult());
    }
}