package com.tatarinov.helpdesk.dao.impl;

import com.tatarinov.helpdesk.dao.AbstractRepository;
import com.tatarinov.helpdesk.dao.AttachmentDao;
import com.tatarinov.helpdesk.model.entity.Attachment;
import com.tatarinov.helpdesk.model.entity.Ticket;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class AttachmentDaoImpl extends AbstractRepository<Attachment> implements AttachmentDao {

    protected AttachmentDaoImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public Optional<Attachment> getAttachmentForTicket(Ticket ticket) {

        return Optional.ofNullable((Attachment) sessionFactory
                .getCurrentSession()
                .createQuery("from Attachment a where a.ticket = :ticket")
                .setParameter("ticket", ticket)
                .getSingleResult());
    }

    @Override
    public void deleteAttachment(long id) {

        sessionFactory
                .getCurrentSession()
                .delete(this.getEntity(id));
    }
}