package com.tatarinov.helpdesk.dao.impl;

import com.tatarinov.helpdesk.dao.AbstractRepository;
import com.tatarinov.helpdesk.dao.CommentDao;
import com.tatarinov.helpdesk.model.entity.Comment;
import com.tatarinov.helpdesk.model.entity.Ticket;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class CommentDaoImpl extends AbstractRepository<Comment> implements CommentDao {

    protected CommentDaoImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public List<Comment> getAllCommentsForTicket(Ticket ticket) {

        return sessionFactory
                .getCurrentSession()
                .createQuery("from Comment c where c.ticket = :ticket")
                .setParameter("ticket", ticket)
                .getResultList();
    }
}