package com.tatarinov.helpdesk.dao.impl;

import com.tatarinov.helpdesk.dao.AbstractRepository;
import com.tatarinov.helpdesk.dao.HistoryDao;
import com.tatarinov.helpdesk.model.entity.History;
import com.tatarinov.helpdesk.model.entity.Ticket;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class HistoryDaoImpl extends AbstractRepository<History> implements HistoryDao {

    protected HistoryDaoImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public List<History> getAllHistoriesForTicket(Ticket ticket) {

        return sessionFactory
                .getCurrentSession()
                .createQuery("from History h where h.ticket = :ticket")
                .setParameter("ticket", ticket)
                .getResultList();
    }
}