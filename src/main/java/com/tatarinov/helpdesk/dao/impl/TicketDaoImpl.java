package com.tatarinov.helpdesk.dao.impl;

import com.tatarinov.helpdesk.dao.AbstractRepository;
import com.tatarinov.helpdesk.dao.TicketDao;
import com.tatarinov.helpdesk.model.entity.Ticket;
import com.tatarinov.helpdesk.model.entity.User;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class TicketDaoImpl extends AbstractRepository<Ticket> implements TicketDao {

    protected TicketDaoImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public List<Ticket> getAllTicketsForEmployee(User user) {

        return sessionFactory
                .getCurrentSession()
                .createQuery("from Ticket t where t.userOwner = :userOwner")
                .setParameter("userOwner", user)
                .getResultList();
    }

    @Override
    public List<Ticket> getAllTicketsForEngineer(User user) {

        return sessionFactory
                .getCurrentSession()
                .createQuery("from Ticket t where t.state = :stateApproved " +
                        "or (t.userAssignee = :userAssignee and " +
                        "(t.state = :stateInProgress or t.state = :stateDone))")
                .setParameter("userAssignee", user)
                .setParameter("stateApproved", "APPROVED")
                .setParameter("stateInProgress", "IN_PROGRESS")
                .setParameter("stateDone", "DONE")
                .getResultList();
    }

    @Override
    public List<Ticket> getAllTicketsForManager(User user) {

        return sessionFactory
                .getCurrentSession()
                .createQuery("from Ticket t where t.userOwner = :userOwner " +
                        "or t.state = :stateNew " +
                        "or (t.userApprover = :userApprover and not " +
                        "(t.state = :stateNew or t.state = :stateDraft))")
                .setParameter("userOwner", user)
                .setParameter("userApprover", user)
                .setParameter("stateNew", "NEW")
                .setParameter("stateDraft", "DRAFT")
                .getResultList();
    }
}