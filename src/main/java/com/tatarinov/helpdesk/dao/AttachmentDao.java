package com.tatarinov.helpdesk.dao;

import com.tatarinov.helpdesk.model.entity.Attachment;
import com.tatarinov.helpdesk.model.entity.Ticket;

import java.util.Optional;

public interface AttachmentDao {
    Optional<Attachment> getEntity(long id);
    Optional<Attachment> getAttachmentForTicket(Ticket ticket);
    void saveEntity(Attachment attachment);
    void deleteAttachment(long id);
}
