package com.tatarinov.helpdesk.dao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.GenericTypeResolver;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public abstract class AbstractRepository<T> {

    private final Class<T> type = (Class<T>) GenericTypeResolver.resolveTypeArgument(getClass(), AbstractRepository.class);
    protected final SessionFactory sessionFactory;

    @Autowired
    protected AbstractRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public Optional<T> getEntity(long id) {

        return Optional.ofNullable(sessionFactory
                .getCurrentSession()
                .get(type, id));
    }

    public void saveEntity(T entity) {

        sessionFactory
                .getCurrentSession()
                .saveOrUpdate(entity);
    }
}