package com.tatarinov.helpdesk.dao;

import com.tatarinov.helpdesk.model.entity.User;

import java.util.List;
import java.util.Optional;

public interface UserDao {
    List<User> getAllUsers();
    Optional<User> getEntity(long id);
    Optional<User> getUserByEmail(String email);
    List<User> getAllManagers();
    List<User> getAllEngineers();
}