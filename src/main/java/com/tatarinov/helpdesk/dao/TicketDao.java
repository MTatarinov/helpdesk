package com.tatarinov.helpdesk.dao;

import com.tatarinov.helpdesk.model.entity.Ticket;
import com.tatarinov.helpdesk.model.entity.User;

import java.util.List;
import java.util.Optional;

public interface TicketDao {
    List<Ticket> getAllTicketsForEmployee(User user);
    List<Ticket> getAllTicketsForEngineer(User user);
    List<Ticket> getAllTicketsForManager(User user);
    Optional<Ticket> getEntity(long id);
    void saveEntity(Ticket ticket);
}
