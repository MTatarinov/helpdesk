package com.tatarinov.helpdesk.dao;

import com.tatarinov.helpdesk.model.entity.Comment;
import com.tatarinov.helpdesk.model.entity.Ticket;

import java.util.List;
import java.util.Optional;

public interface CommentDao {
    List<Comment> getAllCommentsForTicket(Ticket ticket);
    Optional<Comment> getEntity(long id);
    void saveEntity(Comment comment);
}
