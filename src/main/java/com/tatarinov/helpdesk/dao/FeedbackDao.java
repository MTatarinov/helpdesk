package com.tatarinov.helpdesk.dao;

import com.tatarinov.helpdesk.model.entity.Feedback;
import com.tatarinov.helpdesk.model.entity.Ticket;

import java.util.Optional;

public interface FeedbackDao {
    Optional<Feedback> getFeedbackForTicket(Ticket ticket);
    Optional<Feedback> getEntity(long id);
    void saveEntity(Feedback feedback);
}