package com.tatarinov.helpdesk.dao;

import com.tatarinov.helpdesk.model.entity.History;
import com.tatarinov.helpdesk.model.entity.Ticket;

import java.util.List;
import java.util.Optional;

public interface HistoryDao {
    List<History> getAllHistoriesForTicket(Ticket ticket);
    Optional<History> getEntity(long id);
    void saveEntity(History history);
}
