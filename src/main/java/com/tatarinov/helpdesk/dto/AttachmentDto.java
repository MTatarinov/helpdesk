package com.tatarinov.helpdesk.dto;

import lombok.Data;

@Data
public class AttachmentDto {

    private Long id;
    private String name;
    private byte[] blob;
    private String contentType;
}
