package com.tatarinov.helpdesk.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
public class CommentDto {

    private Long id;

    @NotEmpty
    @Size(max = 500, message = "Comment must be less than {max} characters")
    private String text;
}
