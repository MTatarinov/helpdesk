package com.tatarinov.helpdesk.dto;

import lombok.Data;
import org.hibernate.validator.constraints.Range;

@Data
public class FeedbackDto {

    private Long id;

    @Range(min = 1, max = 5, message = "The value must be between {min} and {max}")
    private byte rate;
    private String text;
}
