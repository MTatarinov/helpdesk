package com.tatarinov.helpdesk.dto;

import lombok.Data;

@Data
public class HistoryDto {

    private Long id;
    private String action;
    private String description;
}
