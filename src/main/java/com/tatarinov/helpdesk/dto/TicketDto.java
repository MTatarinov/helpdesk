package com.tatarinov.helpdesk.dto;

import com.tatarinov.helpdesk.model.enums.Category;
import com.tatarinov.helpdesk.model.enums.State;
import com.tatarinov.helpdesk.model.enums.Urgency;
import com.tatarinov.helpdesk.validation.ValueOfEnum;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
public class TicketDto {

    private Long id;

    @NotEmpty
    @Size(max = 100, message = "Name must be less than {max} characters")
    private String name;

    @NotEmpty
    @Size(max = 500, message = "Description must be less than {max} characters")
    private String description;

    @Pattern(regexp = "^\\d{2}/\\d{2}/\\d{4}\\b",
             message = "The date must match the pattern dd/MM/yyyy")
    private String desiredResolutionDate;

    @ValueOfEnum(enumClass = State.class)
    private String state;

    @ValueOfEnum(enumClass = Category.class)
    private String category;

    @ValueOfEnum(enumClass = Urgency.class)
    private String urgency;
}