package com.tatarinov.helpdesk.dto;

import com.tatarinov.helpdesk.model.enums.Role;
import com.tatarinov.helpdesk.validation.ValueOfEnum;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

@Data
public class UserDto {

    private Long id;
    private String firstName;
    private String lastName;

    @ValueOfEnum(enumClass = Role.class)
    private String role;

    @Size(max = 100, message = "Email must be less than {max} characters")
    @Email
    private String email;

    @Size(min = 6, max = 20, message = "Password must be from {min} to {max} characters")
    private String password;
}