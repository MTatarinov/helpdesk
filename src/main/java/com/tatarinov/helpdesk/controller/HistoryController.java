package com.tatarinov.helpdesk.controller;

import com.tatarinov.helpdesk.dto.HistoryDto;
import com.tatarinov.helpdesk.service.HistoryService;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Log4j2
@AllArgsConstructor
@RestController
@RequestMapping("api/v1/")
public class HistoryController {

    private final HistoryService historyService;

    @GetMapping("/tickets/{ticketId}/histories")
    public ResponseEntity<List<HistoryDto>> getAllHistoriesForTicket(@PathVariable long ticketId) {
        log.info("Getting histories for ticket with id = " + ticketId);
        return new ResponseEntity<>(historyService.getAllHistoriesForTicket(ticketId),
                HttpStatus.OK);
    }

    @GetMapping("/tickets/histories/{historyId}")
    public ResponseEntity<HistoryDto> getHistory(@PathVariable long historyId) {
        log.info("Getting history with id = " + historyId);
        return new ResponseEntity<>(historyService.getHistory(historyId), HttpStatus.OK);
    }

    @PostMapping("/tickets/{ticketId}/histories")
    public ResponseEntity<?> addHistory(@PathVariable long ticketId,
                                        @RequestBody HistoryDto historyDto) {
        log.info("Adding history to the ticket with id = " + ticketId);
        historyService.saveHistory(historyDto, ticketId);
        log.info("History has been added");
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}