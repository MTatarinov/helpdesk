package com.tatarinov.helpdesk.controller;

import com.tatarinov.helpdesk.dto.AttachmentDto;
import com.tatarinov.helpdesk.service.AttachmentService;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Log4j2
@AllArgsConstructor
@RestController
@RequestMapping("api/v1/")
public class AttachmentController {

    private final AttachmentService attachmentService;

    @GetMapping("/tickets/{ticketId}/attachments")
    public ResponseEntity<AttachmentDto> getAttachmentForTicket(@PathVariable long ticketId) {
        log.info("Getting attachment for ticket with id = " + ticketId);
        return new ResponseEntity<>(attachmentService.getAttachmentForTicket(ticketId),
                HttpStatus.OK);
    }

    @GetMapping("/tickets/attachments/{attachmentId}")
    public ResponseEntity<AttachmentDto> getAttachment(@PathVariable long attachmentId) {
        log.info("Getting attachment with id = " + attachmentId);
        return new ResponseEntity<>(attachmentService.getAttachment(attachmentId),
                HttpStatus.OK);
    }

    @GetMapping("/tickets/attachments/{attachmentId}/download")
    public ResponseEntity<ByteArrayResource> getFile(@PathVariable long attachmentId) {
        log.info("Getting file in attachment with id = " + attachmentId);
        AttachmentDto attachmentDto = attachmentService.getAttachment(attachmentId);
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(attachmentDto.getContentType()))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment:filename=" + attachmentDto.getName())
                .body(new ByteArrayResource(attachmentDto.getBlob()));
    }

    @PostMapping("/tickets/{ticketId}/attachments")
    public ResponseEntity<?> addAttachment(@PathVariable long ticketId,
                                           @RequestParam("document") MultipartFile file) throws IOException {
        log.info("Adding attachment to the ticket with id = " + ticketId);
        attachmentService.saveAttachment(file, ticketId);
        log.info("Attachment has been added");
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @DeleteMapping("/tickets/attachments/{attachmentId}")
    public ResponseEntity<?> deleteAttachment(@PathVariable long attachmentId) {
        attachmentService.deleteAttachment(attachmentId);
        log.info("Attachment with id = " + attachmentId + " has been updated");
        return new ResponseEntity<>(HttpStatus.OK);
    }
}