package com.tatarinov.helpdesk.controller;

import com.tatarinov.helpdesk.dto.FeedbackDto;
import com.tatarinov.helpdesk.service.FeedbackService;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Log4j2
@AllArgsConstructor
@RestController
@RequestMapping("api/v1/")
public class FeedbackController {

    private final FeedbackService feedbackService;

    @GetMapping("/tickets/{ticketId}/feedbacks")
    public ResponseEntity<FeedbackDto> getFeedbackForTicket(@PathVariable long ticketId) {
        log.info("Getting feedback for ticket with id = " + ticketId);
        return new ResponseEntity<>(feedbackService.getFeedbackForTicket(ticketId), HttpStatus.OK);
    }

    @GetMapping("/tickets/feedbacks/{feedbackId}")
    public ResponseEntity<FeedbackDto> getFeedback(@PathVariable long feedbackId) {
        log.info("Getting feedback with id = " + feedbackId);
        return new ResponseEntity<>(feedbackService.getFeedback(feedbackId), HttpStatus.OK);
    }

    @PostMapping("/tickets/{ticketId}/feedbacks")
    public ResponseEntity<?> addFeedback(@PathVariable long ticketId,
                                         @Valid @RequestBody FeedbackDto feedbackDto,
                                         BindingResult bindingResult) throws Exception {
        log.info("Adding feedback to the ticket with id = " + ticketId);
        if (bindingResult.hasErrors()) {
            log.info("Errors validation: " + bindingResult.getFieldError());
            throw new Exception(bindingResult.getFieldError().getDefaultMessage());
        }
        feedbackService.saveFeedback(feedbackDto, ticketId);
        log.info("Feedback has been added");
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping("/tickets/feedbacks/{feedbackId}")
    public ResponseEntity<FeedbackDto> updateFeedback(@PathVariable long feedbackId,
                                                      @RequestBody FeedbackDto feedbackDto,
                                                      BindingResult bindingResult) throws Exception {
        if (bindingResult.hasErrors()) {
            log.info("Errors validation: " + bindingResult.getFieldError());
            throw new Exception(bindingResult.getFieldError().getDefaultMessage());
        }
        feedbackService.updateFeedback(feedbackDto, feedbackId);
        feedbackDto.setId(feedbackId);
        log.info("Feedback has been updated");
        return new ResponseEntity<>(feedbackDto, HttpStatus.OK);
    }
}