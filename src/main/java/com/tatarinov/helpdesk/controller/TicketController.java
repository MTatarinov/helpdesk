package com.tatarinov.helpdesk.controller;

import com.tatarinov.helpdesk.dto.TicketDto;
import com.tatarinov.helpdesk.service.TicketService;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Log4j2
@AllArgsConstructor
@RestController
@RequestMapping("/api/v1/")
public class TicketController {

    private final TicketService ticketService;

    @GetMapping("/users/{userId}/tickets")
    public ResponseEntity<List<TicketDto>> getAllTicketsForUser(@PathVariable long userId) {
        log.info("Getting tickets for user with id = " + userId);
        return new ResponseEntity<>(ticketService.getAllTicketsForUser(userId), HttpStatus.OK);
    }

    @GetMapping("/tickets/{ticketId}")
    public ResponseEntity<TicketDto> getTicket(@PathVariable long ticketId) {
        log.info("Getting ticket with id = " + ticketId);
        return new ResponseEntity<>(ticketService.getTicket(ticketId), HttpStatus.OK);
    }

    @PostMapping("/users/{userId}/tickets")
    public ResponseEntity<?> addTicket(@PathVariable long userId,
                                       @Valid @RequestBody TicketDto ticketDto,
                                       BindingResult bindingResult) throws Exception {
        log.info("Adding ticket to the user with id = " + userId);
        if (bindingResult.hasErrors()) {
            log.info("Errors validation: " + bindingResult.getFieldError());
            throw new Exception(bindingResult.getFieldError().getDefaultMessage());
        }
        ticketService.saveTicket(ticketDto, userId);
        log.info("Ticket has been added");
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping("/tickets/{ticketId}")
    public ResponseEntity<TicketDto> updateTicket(@PathVariable long ticketId,
                                                  @Valid @RequestBody TicketDto ticketDto,
                                                  BindingResult bindingResult) throws Exception {
        if (bindingResult.hasErrors()) {
            log.info("Errors validation: " + bindingResult.getFieldError());
            throw new Exception(bindingResult.getFieldError().getDefaultMessage());
        }
        ticketService.updateTicket(ticketDto, ticketId);
        ticketDto.setId(ticketId);
        log.info("Ticket has been updated");
        return new ResponseEntity<>(ticketDto, HttpStatus.OK);
    }

    @PutMapping("/users/{userId}/tickets/{ticketId}")
    public ResponseEntity<TicketDto> updateTicketState(@PathVariable long ticketId,
                                                       @PathVariable long userId,
                                                       @Valid @RequestBody TicketDto ticketDto,
                                                       BindingResult bindingResult) throws Exception {
        if (bindingResult.hasErrors()) {
            log.info("Errors validation: " + bindingResult.getFieldError());
            throw new Exception(bindingResult.getFieldError().getDefaultMessage());
        }
        ticketService.updateTicketState(ticketDto, ticketId, userId);
        ticketDto.setId(ticketId);
        log.info("Ticket has been updated");
        return new ResponseEntity<>(ticketDto, HttpStatus.OK);
    }
}