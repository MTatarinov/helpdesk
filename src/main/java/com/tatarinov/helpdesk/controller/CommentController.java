package com.tatarinov.helpdesk.controller;

import com.tatarinov.helpdesk.dto.CommentDto;
import com.tatarinov.helpdesk.service.CommentService;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Log4j2
@AllArgsConstructor
@RestController
@RequestMapping("api/v1/")
public class CommentController {

    private final CommentService commentService;

    @GetMapping("/tickets/{ticketId}/comments")
    public ResponseEntity<List<CommentDto>> getAllCommentsForTicket(@PathVariable long ticketId) {
        log.info("Getting comments for ticket with id = " + ticketId);
        return new ResponseEntity<>(commentService.getAllCommentsForTicket(ticketId),
                HttpStatus.OK);
    }

    @GetMapping("/tickets/comments/{commentId}")
    public ResponseEntity<CommentDto> getComment(@PathVariable long commentId) {
        log.info("Getting comment with id = " + commentId);
        return new ResponseEntity<>(commentService.getComment(commentId), HttpStatus.OK);
    }

    @PostMapping("/tickets/{ticketId}/comments")
    public ResponseEntity<?> addComment(@PathVariable long ticketId,
                                        @Valid @RequestBody CommentDto commentDto,
                                        BindingResult bindingResult) throws Exception {
        log.info("Adding comment to the ticket with id = " + ticketId);
        if (bindingResult.hasErrors()) {
            log.info("Errors validation: " + bindingResult.getFieldError());
            throw new Exception(bindingResult.getFieldError().getDefaultMessage());
        }
        commentService.saveComment(commentDto, ticketId);
        log.info("Comment has been added");
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping("/tickets/comments/{commentId}")
    public ResponseEntity<CommentDto> updateComment(@PathVariable long commentId,
                                                    @Valid @RequestBody CommentDto commentDto,
                                                    BindingResult bindingResult) throws Exception {
        if (bindingResult.hasErrors()) {
            log.info("Errors validation: " + bindingResult.getFieldError());
            throw new Exception(bindingResult.getFieldError().getDefaultMessage());
        }
        commentService.updateComment(commentDto, commentId);
        commentDto.setId(commentId);
        log.info("Comment has been updated");
        return new ResponseEntity<>(commentDto, HttpStatus.OK);
    }
}