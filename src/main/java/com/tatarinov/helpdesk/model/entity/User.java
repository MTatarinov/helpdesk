package com.tatarinov.helpdesk.model.entity;

import com.tatarinov.helpdesk.model.enums.Role;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "role_id")
    @Enumerated(EnumType.STRING)
    private Role role;

    @Column(name = "email", length = 100)
    private String email;

    @Column(name = "password", length = 20)
    private String password;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private List<Feedback> feedbacks;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private List<Comment> comments;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private List<History> histories;

    @OneToMany(mappedBy = "userAssignee", fetch = FetchType.LAZY)
    private List<Ticket> ticketsAssignee;

    @OneToMany(mappedBy = "userOwner", fetch = FetchType.LAZY)
    private List<Ticket> ticketsOwner;

    @OneToMany(mappedBy = "userApprover", fetch = FetchType.LAZY)
    private List<Ticket> ticketsApprover;
}