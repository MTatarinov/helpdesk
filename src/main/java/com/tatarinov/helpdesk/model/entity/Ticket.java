package com.tatarinov.helpdesk.model.entity;

import com.tatarinov.helpdesk.model.enums.Category;
import com.tatarinov.helpdesk.model.enums.State;
import com.tatarinov.helpdesk.model.enums.Urgency;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Data
@Entity
@Table(name = "tickets")
public class Ticket {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name", length = 100)
    private String name;

    @Column(name = "description", length = 500)
    private String description;

    @Column(name = "created_on")
    @CreationTimestamp
    @Temporal(TemporalType.DATE)
    private Date createdOn;

    @Column(name = "desired_resolution_date")
    @Temporal(TemporalType.DATE)
    private Date desiredResolutionDate;

    @ManyToOne
    @JoinColumn(name = "assignee_id")
    private User userAssignee;

    @ManyToOne
    @JoinColumn(name = "owner_id")
    private User userOwner;

    @ManyToOne
    @JoinColumn(name = "approver_id")
    private User userApprover;

    @Column(name = "state_id")
    @Enumerated(EnumType.STRING)
    private State state;

    @Column(name = "category_id")
    @Enumerated(EnumType.STRING)
    private Category category;

    @Column(name = "urgency_id")
    @Enumerated(EnumType.STRING)
    private Urgency urgency;

    @OneToOne(mappedBy = "ticket")
    private Feedback feedback;

    @OneToOne(mappedBy = "ticket")
    private Attachment attachment;

    @OneToMany(mappedBy = "ticket")
    private List<History> histories;

    @OneToMany(mappedBy = "ticket")
    private List<Comment> comments;
}