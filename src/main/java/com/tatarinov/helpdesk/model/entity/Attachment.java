package com.tatarinov.helpdesk.model.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "attachments")
public class Attachment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Lob
    @Column(name = "blob")
    private byte[] blob;

    @OneToOne
    @JoinColumn(name = "ticket_id")
    private Ticket ticket;

    @Column(name = "name")
    private String name;

    @Column(name = "content_type")
    private String contentType;
}