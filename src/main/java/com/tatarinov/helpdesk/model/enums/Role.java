package com.tatarinov.helpdesk.model.enums;

public enum Role {
    EMPLOYEE,
    MANAGER,
    ENGINEER
}