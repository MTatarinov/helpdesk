package com.tatarinov.helpdesk.model.enums;

public enum Urgency {
    CRITICAL,
    HIGH,
    AVERAGE,
    LOW
}