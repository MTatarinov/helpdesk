package com.tatarinov.helpdesk.model.enums;

public enum Category {
    APPLICATION_SERVICES,
    BENEFITS_PAPER_WORK,
    HARDWARE_SOFTWARE,
    PEOPLE_MANAGEMENT,
    SECURITY_ACCESS,
    WORKPLACES_FACILITIES
}
