package com.tatarinov.helpdesk.model.enums;

public enum State {
    DRAFT,
    NEW,
    APPROVED,
    DECLINED,
    IN_PROGRESS,
    DONE,
    CANCELLED
}